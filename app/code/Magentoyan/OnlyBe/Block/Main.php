<?php

namespace Magentoyan\OnlyBe\Block;

use Magento\Framework\View\Element\Template;
use Magento\Cms\Model\Page as CmsPage;
use Magento\Framework\App\Request\Http as Request;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Locale\Resolver as LocaleResolver;

class Main extends Template {

    private $request;
    private $_cmsPage;
    private $_storeManagerInterface;
    private $_localeResolver;

    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, Request $request, CmsPage $cmsPage, StoreManagerInterface $storeManagerInterface, LocaleResolver $localeResolver, array $data = []
    ) {
        parent::__construct($context, $data);

        $this->request = $request;
        $this->_cmsPage = $cmsPage;
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->_localeResolver = $localeResolver;
    }

    protected function _prepareLayout() {
        return parent::_prepareLayout();
    }

    public function getLinkRel() {

        $controllerName = $this->request->getControllerName();
        $actionName = $this->request->getActionName();
        $moduleName = $this->request->getModuleName();

        $skipAlternateLink = true;

        if ($controllerName == 'page' && $actionName == 'view' && $moduleName == 'cms'
        ) {

            $store = $this->_storeManagerInterface->getStore();  // Get Store ID

            $storeId = $store->getStoreId();

            $storeLanguage = str_replace('_', '-', strtolower($this->_localeResolver->getLocale()));

            $cmsPageUrl = $this->_cmsPage->getIdentifier(); //Get Current CMS Page Identifier


            $storeViews = $this->_cmsPage->getStoreId();

            if (in_array(0, $storeViews))/* All Store views */
                $skipAlternateLink = false;

            if (in_array($storeId, $storeViews) && count($storeViews) > 1) /* This page is not only for me */
                $skipAlternateLink = false;

            $baseUrl = $store->getBaseUrl();
        }

        if ($skipAlternateLink)
            return '';

        $linkRel = '<link rel="alternate" hreflang="' . $storeLanguage . '" href="' . $baseUrl . $cmsPageUrl . '" />';

        return $linkRel;
    }

}
